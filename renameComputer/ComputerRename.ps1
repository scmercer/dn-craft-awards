#Delaware North Companies 2015
#Coded by Nicholas Barning

    [void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
    [void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
    
    # Set the size of your form
    $Form = New-Object System.Windows.Forms.Form
    $Form.width = 325
    $Form.height = 350
    $Form.Text = ”Name Computer"
    $form.StartPosition = "CenterScreen"
  
 
    # Set the font of the text to be used within the form
    $Font = New-Object System.Drawing.Font("Arial",10)
    $Form.Font = $Font
 
    # Create a group that will contain your radio buttons
    $MyGroupBox = New-Object System.Windows.Forms.GroupBox
    $MyGroupBox.Location = '40,20'
    $MyGroupBox.size = '100,200'
    $MyGroupBox.text = "Unit:"
    
    $Label = New-Object System.windows.forms.label
    $Label.Location = '160,20'
    $Label.Text = "Site:"
    
    # Create the collection of radio buttons
    $RadioButton1 = New-Object System.Windows.Forms.RadioButton
    $RadioButton1.Location = '20,30'
    $RadioButton1.size = '50,20'
    $RadioButton1.Checked = "$true"
    $RadioButton1.Text = "HQ"    

    $RadioButton2 = New-Object System.Windows.Forms.RadioButton
    $RadioButton2.Location = '20,60'
    $RadioButton2.size = '50,20'
    #$RadioButton2.Checked = "$false"
    $RadioButton2.Text = "PR"
     
    $RadioButton3 = New-Object System.Windows.Forms.RadioButton
    $RadioButton3.Location = '20,90'
    $RadioButton3.size = '50,20'
    #$RadioButton3.Checked = $false
    $RadioButton3.Text = "SS"
    
    $RadioButton4 = New-Object System.Windows.Forms.RadioButton
    $RadioButton4.Location = '20,120'
    $RadioButton4.size = '50,20'
    #$RadioButton4.Checked = "false"
    $RadioButton4.Text = "THS"
    
    $RadioButton5 = New-Object System.Windows.Forms.RadioButton
    $RadioButton5.Location = '20,150'
    $RadioButton5.size = '50,20'
    #$RadioButton5.Checked = "false"
    $RadioButton5.Text = "GE"
   
    # Add an OK button
    # Thanks to J.Vierra for simplifing the use of buttons in forms
    $OKButton = new-object System.Windows.Forms.Button
    $OKButton.Location = '50,250'
    $OKButton.Size = '100,25' 
    $OKButton.Text = 'OK'
    $OKButton.DialogResult=[System.Windows.Forms.DialogResult]::OK
 
    #Add a cancel button
    $CancelButton = new-object System.Windows.Forms.Button
    $CancelButton.Location = '175,250'
    $CancelButton.Size = '100,25'
    $CancelButton.Text = "Cancel"
    $CancelButton.DialogResult=[System.Windows.Forms.DialogResult]::Cancel
    
    $ComboBox = New-Object System.Windows.Forms.ComboBox
    $ComboBox.location = '160,45'
    $ComboBox.Size = '100,25'
    $ComboBox.Sorted = $true
    #$ComboBox.Scroll = $true
          
    # Add all the Form controls on one line 
    $form.Controls.AddRange(@($MyGroupBox,$OKButton,$CancelButton,$ComboBox,$Label))
 
    # Add all the GroupBox controls on one line
    $MyGroupBox.Controls.AddRange(@($Radiobutton1,$RadioButton2,$RadioButton3,$RadioButton4,$RadioButton5))
    
    # Assign the Accept and Cancel options in the form to the corresponding buttons
    $form.AcceptButton = $OKButton
    $form.CancelButton = $CancelButton
 
    # Activate the form
    $form.Activate()
    
    # Adding and editing locations to the range of entries can be done here
    $RadioButton1.Add_Click({
    If ($RadioButton1.checked)
        {
        $ComboBox.enabled = $false
        $Prefix = "HQ"}        
        })
     $RadioButton2.Add_Click({
    If ($RadioButton2.checked)
        {
        $ComboBox.enabled = $true
        $ComboBox.items.clear()
        $ComboBox.items.AddRange(@("YOS","SEQ","KSC","KC","FR","GEN","GC","HAR","NF","SH","WF","YS","GP"))
        $Prefix = "PR"}
        })
     $RadioButton3.Add_Click({
    If ($RadioButton3.checked)
        {
        $ComboBox.enabled = $true
        $ComboBox.items.clear()
        $ComboBox.items.AddRange(@("CHIBB","CARFB","PHXPS","STLFB","BALBB","BOS","FNC","RWS","CARFB","CINBB","CLEVBB","NWA","DETBB","EDM","GB","HARBB","KC","MEMBB","MINBB","NASH","PRO","RBA","SDBB","SEA","STLBB","STLFB","TXBB","WIS"))
        $Prefix = "SS"}
    })
    $RadioButton4.Add_Click({
    If ($RadioButton4.checked)
        {
        $ComboBox.enabled = $true
        $ComboBox.items.clear()
        $ComboBox.items.AddRange(@("DET","ATL","AUS","BUF","CLAR","BOI","CHS","DEN","FTL","LAX","GWB","MEM","NASH","NEW","PEMB","OKC","NO","ONT","RICH"))
        $Prefix = "THS"}
        })
     $RadioButton5.Add_Click({
    If ($RadioButton5.checked)
        {
        $ComboBox.enabled = $true
        $ComboBox.items.clear()
        $ComboBox.items.AddRange(@("AGP","CHO","DBKC","GH","FL","HGR","JUM","PGP","SGR"))
        $Prefix = "GE"}
    })

    $form.ShowDialog()
    

# The following line allows for users to either select from the drop down list OR enter a new site in the text box
($Site = $ComboBox.text) -or ($Site = $ComboBox.SelectedItem)

# Get a random number between 100 and 999
$Number = Get-Random -minimum 100 -maximum 999

# Search the registry for the hardware description
$model = (Get-ItemProperty 'HKLM:\HARDWARE\DESCRIPTION\System\BIOS').SystemProductName

# Based on the hardware description taken, assign a value
if ($model -like "*EliteBook*")
    {
    $model = "W7NB"
    }
else 
    {
    if ($model -like "*Folio*")
    {
    $model = "W7NB"
    }    
    else 
        {
        if ($model -like "*5700*")
        {
        $model = "W7PC"
        }
        else
            {            
            if ($model -like "*5800*")
            {
            $model = "W7PC"
            }
                else
                    {            
                    if ($model -like "*Surface*")
                    {
                    $model = "W81SUR"
                    }
           }
          }
         }
        }
                   
$Computername = "$Prefix" + "$Site" + "$Model" + "$Number"

# The path needed to pull the list of computers from text in AD
$List = Get-Content "\\corpnas001\it\ComputerName\List.txt" -Filter * | select-string -Pattern "$Computername"

# Loop through the names
if (!$List) 
{
$Computername = $Computername
}
else 
{
    if ($List)
    { 
       Do
        {
        $Number++
        $Computername = "$Prefix" + "Site" + "$Model" + "$Number"
        $Computername = $Computername
        }
        While ($List = Get-Content "\\corpnas001\it\ComputerName\List.txt" -Filter * | select-string -Pattern "$Computername")
    }       
}

# Use GWMI and then rename the computer and restart
$name = Get-WmiObject Win32_ComputerSystem

$name.Rename($Computername)

restart-computer