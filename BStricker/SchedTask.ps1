#################################################################################
#																			    #
#  Script Title: Create a Scheduled Task	    							    #
#  File Name: SchedTask.ps1					       							    #
#  Written By: Brian Stricker  7/30/2015								        #
#  Description: PowerShell script to create a scheduled task and copy required  #
#   files                                                                       #
#  Requirements: PowerShell 3.0 or better                                       #
#  							                 								    #
#				                                        #
#  Peer Review done by: 									                    #
#																			    #
#################################################################################
#																				#
# Determine Powershell Version													#
#																				#
$version= $PSVersionTable.PSVersion.Major
if ($version -lt 3) {Write-Host "This script requires PowerShell 3.0 or greater"}
if ($version -lt 3) {exit} 
#																				#
#################################################################################
#                                                                                #
##################################################################################
#                                                                                #
# Create Scheduled Task for each server in the data file and copy needed files   #
#                                                                                #
Import-Csv "\\awswsus001\C$\PSScripts\awsservers.csv" |
ForEach-Object `
{
    $server= $_.server
    $DOW= $_.DOW
    $TOD= $_.TOD
    $path= "\\$server\C$\PSScripts"

    New-Item -Path $path -ItemType directory -Force
    Copy-Item "\\awswsus001\C$\PSScripts\wu.ps1" $path -Force
    Copy-Item "\\awswsus001\C$\PSScripts\\PSWindowsUpdate\"  "\\$server\C$\windows\system32\windowspowershell\v1.0\modules\" -Recurse -Force    
    
    $task= New-ScheduledTaskAction -Execute 'powershell.exe' -Argument '-NoProfile -ExecutionPolicy Bypass -File C:\PSScripts\wu.ps1'
    $start= New-ScheduledTaskTrigger -Weekly -DaysOfWeek $DOW -At $TOD
    Register-ScheduledTask -CimSession $server -Action $task -Trigger $start -User System -TaskName "Install Windows Updates" -Description "PS Script to Automate Windows Updates"
}

