<#
.Synopsis
	Script to Search Active Directory and return a list of computers based on Server Type and export it to a CSV.
.Description
	Based on a defines set of variables the following script will use the ActiveDirectory Module to search your current AD infrastucutre. 
	The Returned results will be output to a Variable Defined folder with a time and date stamp that it ran. It will also produce an exectution
	log in the same folder . If an error occurs an email will be sent to the defined user using a defines SMTP Server.
.Notes
	File Name 		: q3Challenge.ps1
	Author			: Rob Preta
	Creation Date	: October 14th 2015

Change Log:
10/14/15 - Inception 
10/15/15 
    - Forced RunLog Path creation
    - Removed repeat timestamp variable from finally statement
    - Remove split-path option from $FileLocation variable
    - Change file format to txt
#>


#Variable Declarations

#Define File Location for the Results and Run logs
$FileLocation = "c:\Logs\ServerTypes\"

#Define What kind of servers you want to search for IE "Windows Server 2008 R2" , "Windows Server 2012" etc
$ServerType = "Windows Server*"

#Following 2 Variables Set the Time Stamp for the OS Results File
$TimeStamp = Get-Date
$TimeStamp = $TimeStamp.ToString('MM-dd-yyyy_hh-mm-ss')

#Define E-Mail address to send errors from and to
$emailfrom = "scripterror@Delawarenorth.com"
$emailto = "rpreta@Delawarenorth.com"

#Set the SMTP Server below in order to send Errors
$mailserver = "smtprelay.delawarenorth.com"

#Module needed to run the Get-ADComputer computer function and search for servers.
Import-Module ActiveDirectory

#Main Code

#This will look to find all the computers in AD with the $ServerType Variable
Try
{
    Get-ADComputer -Filter {OperatingSystem -Like $ServerType} -Property * | Format-Table Name,OperatingSystem,OperatingSystemServicePack -Wrap -Auto -ErrorAction Stop |out-file $FileLocation\ServerOS_$TimeStamp.txt -Encoding unicode
}
#If there is a failure an email will be sent to the user specified above
Catch
{
    $ErrorMessage = $_.Exception.Message
    $FailedItem = $_.Exception.ItemName
    Send-MailMessage -From $emailfrom -To $emailto -Subject "PowerShell Script Failure!" -SmtpServer $mailserver -Body "We have failed ! $FailedItem. The error message was $ErrorMessage"
    Break
}
#When Complete a log file will be appended that the script ran and the time that it did.
Finally
{
	New-Item -Force -ItemType directory -Path $FileLocation\RunLog
    "The script ran at $TimeStamp" | out-file $FileLocation\RunLog\PSScript.log -append
}